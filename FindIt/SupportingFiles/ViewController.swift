//
//  ViewController.swift
//  FindIt
//
//  Created by a.alami on 07/09/2020.
//  Copyright © 2020 Deep Minds. All rights reserved.
//

import UIKit
class ViewController: UIViewController {
    let welcome: UILabel = {
        let label = UILabel()
        label.font = .systemFont(ofSize: 18)
        label.text = "Welcome :)"
        return label
    }()
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        view.addSubview(welcome)
        welcome.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            welcome.centerYAnchor.constraint(equalTo: view.centerYAnchor),
            welcome.centerXAnchor.constraint(equalTo: view.centerXAnchor)])
    }
}

